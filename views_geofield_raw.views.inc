<?php

/**
 * @file
 * Hooks for Views integration.
 */


/**
 * Implements hook_views_data_alter().
 */
function views_geofield_raw_views_data_alter(&$data) {
  // Clone all geofields to new ones with custom field handler.
  foreach (field_info_fields() as $field) {
    if ($field['module'] != 'geofield' OR $field['storage']['type'] != 'field_sql_storage') {
      continue;
    }
    $table_name = _field_sql_storage_tablename($field);
    $field_name = $field['field_name'];

    $data[$table_name]['raw_data'] = $data[$table_name][$field_name];
    $data[$table_name]['raw_data']['title'] .= t(' (raw)');
    $data[$table_name]['raw_data']['group'] = t('Geofield');
    $data[$table_name]['raw_data']['field']['handler'] = 'views_geofield_raw_handler_field';
  }
}

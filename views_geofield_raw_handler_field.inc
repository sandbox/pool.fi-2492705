<?php

/**
 * @file
 * Geofield raw data field implementation.
 */

class views_geofield_raw_handler_field extends views_handler_field {

  function init(&$view, &$options) {
    parent::init($view, $options);
    // Add field info.
    $this->field_info = field_info_field($this->definition['field_name']);
  }

  /**
   * Exclude from view by default.
   */
  function option_definition() {
    $options = parent::option_definition();
    $options['exclude']['default'] = TRUE;
    return $options;
  }

  /**
   * Exclude default options form.
   */
  function options_form(&$form, &$form_state) {
    $form['message'] = array(
      '#markup' => t('There are no settings for this field.'),
    );
  }

  /**
   * Load the entities for all fields that are about to be displayed.
   */
  function post_execute(&$values) {
    if (empty($values) || empty($this->field_info['columns'])) {
      return;
    }

    $columns = $this->field_info['columns'];
    $column_map = array();
    $field_name = $this->definition['field_name'];
    $column_aliases = $this->aliases;
    $field_alias = $this->field_alias;

    // Build a map from database field to corresponding field column. 
    foreach (array_keys($columns) as $column) {
      $field_key = $field_name . '_' . $column;
      if (isset($column_aliases[$field_key])) {
        $column_map[$column_aliases[$field_key]] = $column;
      }
    }

    foreach ($values as $row_id => &$value) {
      $value->{$field_alias} = array();
      $field_data = &$value->{'field_' . $this->options['id']};
      foreach ($column_map as $field => $column) {
        $value->{$field_alias}[0][$column] = $value->{$field};
      }
    }
  }

  function render($values) {
    return '';
  }
}
